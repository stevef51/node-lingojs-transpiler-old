const lingoJsTranspile = require('./index').lingoJsTranspile;

test(`await Present() is an error`, async () => {
    let lingo = `await Present()`;
    let output = await lingoJsTranspile(lingo);
    expect(output.error).not.toBeNull();
    expect(output.error.message).toContain(`undefined: 'await Present()' is not allowed, remove the 'await'`);
});

test(`Present() is not an error`, async () => {
    let lingo = `Present()`;
    let output = await lingoJsTranspile(lingo);
    expect(output.error).toBeUndefined();
});

/*const bluebird = require('bluebird');
const fs = bluebird.promisifyAll(require('fs'));
const lingoJs = require('./index').lingoTranspile;

async function go() {
    let source = await fs.readFileAsync('./test/Lingo.js', 'utf8');
    let output = await lingoJs(source);

    if (output.error) {
        console.log(output.error);
    } else {
        output.map.file = 'Lingo.min.js';
        output.map.sources = ['Lingo.js'];

        await fs.writeFileAsync('./test/Lingo.min.js', output.code, 'utf8');
        await fs.writeFileAsync('./test/Lingo.js.map', JSON.stringify(output.map), 'utf8');

        console.log(output.code);
    }
}

go();
*/