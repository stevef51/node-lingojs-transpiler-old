/* 
This module exports a single function which takes a LingoJs source code string (which is pure Javascript) and transpiles it to 
pure Javascript (outputting source code & source maps) but making any 'Present(something)' statements special to work in an client side LingoJs player

For more help on the BabelJs docs see

https://babeljs.io/docs/en/
https://github.com/jamiebuilds/babel-handbook/blob/master/translations/en/plugin-handbook.md#babel-traverse

*/

const babel = require('@babel/core');
const b = require('@babel/types');
const generate = require('@babel/generator').default;
const traverse = require('@babel/traverse').default;

const _ = require('lodash');

/* We take the src which could include 

        await fn();
        doSomethingOnce();
        Present(something);

wrap it in an async function to allow embedded await's

    async function Lingo() {
        await fn();
        doSomethingOnce();
        Present(something);
    }

Stage1 & Stage2: and we make an intermediate AST of the following

    async function Lingo() {
        ___await(fn());
        doSomethingOnce();
        await Present(something);
    });

Stage3: we then pass this through Babel with the 'plugin-transform-regenerator' plugin which will do most of the magic for us
in splitting out the (await'ed) Present() statements into a statemachined function, generating ..

    Lingo(() => {
        return regeneratorRuntime.async(function _callee$(_context) {
            while (1) switch (_context.prev = _context.next) {
            case 0:
                ___await(fn());

                doSomethingOnce();
                _context.next = 3;
                return regeneratorRuntime.awrap(Present(something));

            case 3:
            case "end":
                return _context.stop();
            }
        });
    });

Stage4: we then take this AST, replace bits to get

    async function Lingo(_context) {
        while (_context.running()) {
            switch (_context.prev = _context.next) {
                case 0:
                    await fn();

                    doSomethingOnce();
                    _context.next = 3;
                    _context.Present(something);
                    break;

                case 3:
                case "end":
                    return _context.stop();
                }
            }
        }
        return _context.presentArgs();
    });

Stage 5, 6 & 7: However the 'Babel await magic' doesn't quite do exactly what we need since we need every 'Present()' to be
in its own state machine step (read 'case:') so that we can 'previous' to it without re-running unwanted code before it
so we insert a 'case ?:' for each Present(), we have to keep track of _context.next = ? references since we re-index the cases and update
these references aswell to finally generate ..

    async function Lingo(_context) {
        while (_context.running()) {
            switch (_context.prev = _context.next) {
                case 0:
                    await fn();

                    doSomethingOnce();
                    _context.next = 1;
                    break;

                case 1:                           <-- Present has its own case, allows us to 'Previous' back to it without re-executing 'doSomethingOnce()' above
                    _context.next = 2;
                    _context.Present(something);
                    break;

                case 2:
                case "end":
                    return _context.stop();
                }
            }
        }
        return _context.presentArgs();
    });

*/

function throwError(path, msg) {
    let err = path.buildCodeFrameError(msg);
    err.pos = path.node.start;
    err.loc = path.node.loc.start;
    throw err;
}

// This visitor will replace a variable 'oldName' with a pre-built AST reference to 'this.locals.{oldName}'
// This visitor was constructed based on the Babel 
const localiseVariable = {
    ReferencedIdentifier(path, state) {
        if (path.node.name === state.oldName) {
            path.replaceWith(state.ref);
        }
    },

    /*    Scope(path, state) {
            if (!path.scope.bindingIdentifierEquals(state.oldName, state.binding.identifier)) {
                path.skip();
            }
        },
    */
    AssignmentExpression(path, state) {
        const ids = path.getOuterBindingIdentifiers();

        for (const name in ids) {
            if (name === state.oldName) {
                if (state.allowAssign) {
                    path.get('left').replaceWith(state.ref);
                } else {
                    throwError(path, `Cannot assign to a const`);
                }
            }
        }
    }
}

const stage1 = {
    Program: function (path) {
        this.contextId = b.thisExpression();
    },

    // Convert all original 'await fn()' calls into '___await(fn())', we will undo in stage3
    AwaitExpression: function (path) {
        if (b.isCallExpression(path.node.argument) && b.isIdentifier(path.node.argument.callee, { name: 'Present' })) {
            throwError(path, `'await Present()' is not allowed, remove the 'await'`);
        }
        path.replaceWith(b.callExpression(b.identifier('___await'), [path.node.argument]));
    },

    // All locally declared variables and references to them get moved to 'this.locals.?' so we can persist them over state machine calls
    VariableDeclaration: function (path) {
        // Variables declared with 'let' do not get special treatment, these should only be used between Present statements
        if (path.node.kind === 'let') {
            return;
        }
        let locals = [];
        let thisDot = path.node.kind === 'const' ? 'consts' : 'locals';
        path.node.declarations.forEach((decl, index) => {
            // create 'this.locals.?' expression
            let ref = b.memberExpression(b.memberExpression(this.contextId, b.identifier(thisDot)), decl.id);
            let declPath = path.get(`declarations.${index}`);

            // find all references including Assignments to our declared variable and replace with 'this.locals.?'
            declPath.scope.traverse(declPath.scope.block, localiseVariable, {
                oldName: decl.id.name,
                ref,
                allowAssign: path.node.kind !== 'const'
            })

            let declInit = decl.init || b.identifier('undefined');
            if (path.node.kind === 'const') {
                declInit = b.callExpression(b.identifier('Const'), [decl.init]);
            }
            // replace the initialisation aswell
            locals.push(b.expressionStatement(b.assignmentExpression('=', ref, declInit)));
        });

        // this replaces the 'var x, y = 1' statement with 'this.locals.x = undefined; this.locals.y = 1;'
        path.replaceWithMultiple(locals);
    }
}

const stage2 = {
    // Find all 'Present(something)' calls and replace with 'await Present(something)' in prep for BabelJs magic
    CallExpression: function (path) {
        if (path.node.callee.name === 'Present' && !path.node.lingoized) {
            path.node.lingoized = true;
            // Make sure the calling function is async'ed
            let parentFunc = path.findParent(p => p.isFunction() || p.isProgram());
            if (parentFunc.isProgram()) {
                parentFunc.replaceWith(b.program([b.functionDeclaration(b.identifier('Lingo'), [], b.blockStatement(parentFunc.node.body), false, true)]));
            } else {
                parentFunc.node.params = [];
            }
            path.get('callee').replaceWith(b.memberExpression(this.contextId, path.node.callee));
            let statementRoot = path.getStatementParent();
            let expressionPath = statementRoot.get('expression');
            expressionPath.replaceWith(b.awaitExpression(expressionPath.node));
        }
    }
}

// stage after BabelJs magic (stage3) ..
const stage4 = {
    // replace all '___await(fn())' with 'await fn()' to restore original await intentions
    CallExpression: function (path) {
        if (b.isIdentifier(path.node.callee, { name: '___await' })) {
            path.replaceWith(b.awaitExpression(path.node.arguments[0]));
        }
    },

    ReturnStatement: function (path) {
        // replace all 'return regeneratorRuntime.awrap(this.Present(something));' with 'this.Present(something);'
        if (b.isCallExpression(path.node.argument)) {
            let awrapCall =
                b.isMemberExpression(path.node.argument.callee) &&
                    b.isIdentifier(path.node.argument.callee.object, { name: 'regeneratorRuntime' }) &&
                    b.isIdentifier(path.node.argument.callee.property, { name: 'awrap' })
                    ? path.node.argument : null;
            if (awrapCall) {
                let presentCall = awrapCall.arguments[0];
                path.replaceWith(presentCall);
                path.insertAfter(b.breakStatement());

                // Mark this so its easy to find later
                presentCall.presentCall = true;
            }
        }
        // there should be the following code
        //
        // return regeneratorRuntime.async(function _callee(_context) {
        //   while (1) switch(_context.prev = _context.next) {
        //     ...
        //   }
        // }, null, this);
        //
        // we replace the above with
        //
        // while(this.running()) {
        //   switch(this.prev = this.next) {
        //     ...
        //   }
        // }
        // return this.presentArgs();
        if (b.isCallExpression(path.node.argument)) {
            let call = path.node.argument;
            if (b.isMemberExpression(call.callee) && b.isIdentifier(call.callee.object, { name: 'regeneratorRuntime' }) && b.isIdentifier(call.callee.property, { name: 'async' })) {
                let fn = call.arguments.length && call.arguments[0];
                if (fn && b.isFunctionExpression(fn) && b.isIdentifier(fn.id) && fn.params.length) {
                    let body = path.get('argument.arguments.0.body');

                    // Can't simply call rename(fn.params[0].name, 'this') as that generates AST using Identifier('this') which is different to ThisExpression()
                    // so we instead have to manually replace all references ..
                    body.scope.bindings[fn.params[0].name].referencePaths.forEach(rp => {
                        rp.replaceWith(this.contextId);
                    })

                    // Keep track of the switchClause ..
                    this.switchClause = call.arguments[0].body.body[0].body;
                    path.replaceWithMultiple([
                        b.whileStatement(
                            b.callExpression(
                                b.memberExpression(this.contextId, b.identifier('running')),
                                []
                            ),
                            b.blockStatement([this.switchClause])
                        ),
                        b.returnStatement(b.callExpression(b.memberExpression(this.contextId, b.identifier('presentArgs')), []))
                    ]);
                }
            }
        }
    },
    FunctionExpression: function (path) {
        if (path.parentPath != null && path.parentPath.parent != null && path.parentPath.parent.type === 'Program') {
            path.node.async = true;
        }
    }
}

function dump(ast) {
    let source = generate(ast);
    console.log(source.code);
}

const stage5 = {
    // Find all '_context.next = <number>'; statements and note the 'case <number>:' they are referring to
    ExpressionStatement: function (path) {
        if (b.isAssignmentExpression(path.node.expression) &&
            path.node.expression.operator === '=' &&
            b.isMemberExpression(path.node.expression.left) &&
            b.isThisExpression(path.node.expression.left.object) &&
            b.isIdentifier(path.node.expression.left.property, { name: 'next' }) &&
            b.isNumericLiteral(path.node.expression.right)) {
            // Make a note directly on the node ..
            path.node.caseClause = this.caseClauses[path.node.expression.right.value];
        }
    }
}

const stage6 = {
    // Find our 'this.Present(something);' calls (we marked them so easy to find)
    // we create a new 'case ?:' clause for them
    CallExpression: function (path) {
        if (path.node.presentCall) {
            // This is one of our Present calls, need to create a new 'case :' clause for it
            let caseClause = path.findParent(p => b.isSwitchCase(p));

            // Extract body of the new case clause 
            let newCaseBody = caseClause.node.consequent.splice(path.parentPath.key - 1);     // Take the '_context.next = ?;' before it aswell

            // The <test> in 'case <test>:' value can be anything right now since we dont know it until all cases have been rendered and indexed later
            // we hook up the '_context.next = <test>' assignment directly to its 'case <test>:' reference instead, 
            let newCaseTest = -1;
            let contextNextAssignment = b.expressionStatement(b.assignmentExpression('=', b.memberExpression(this.contextId, b.identifier('next')), b.numericLiteral(newCaseTest)));
            contextNextAssignment.caseClause = b.switchCase(b.numericLiteral(newCaseTest), newCaseBody);

            // Close off the previous case clause
            caseClause.node.consequent.push(
                contextNextAssignment,
                b.breakStatement());

            // Insert our new case
            let caseIndex = this.switchClause.cases.indexOf(caseClause.node);
            this.switchClause.cases.splice(caseIndex + 1, 0, contextNextAssignment.caseClause);
        }
    }
}

const stage7 = {
    // Update all marked '_context.next = ?'; assignments with the correct 'case <test>:' value
    ExpressionStatement: function (path) {
        if (path.node.caseClause) {
            // use clauses new test
            path.node.expression.right.value = path.node.caseClause.test.value;
        }
    }
}

const CODE_PREFIX = '(async function ___lingo_module () {\n';
const CODE_POSTFIX = '\n})';

async function lingoJsTranspile(source) {
    try {
        let ast = await babel.parseAsync(`${CODE_PREFIX}${source}${CODE_POSTFIX}`, {
            sourceType: 'script',
            highlightCode: false
        });

        let context = {
            caseClauses: {}
        };
        traverse(ast, stage1, null, context);
        traverse(ast, stage2, null, context);

        // Gen intermediate code, this is stage3 done by Babel
        let babelised = await babel.transformFromAstAsync(ast, null, {
            ast: true,
            code: false,
            plugins: ['@babel/plugin-transform-regenerator']
        });
        ast = babelised.ast;

        traverse(ast, stage4, null, context);

        // find all 'case <test>:' clauses and index them on their current <test> value
        context.switchClause.cases.forEach(caseClause => {
            context.caseClauses[caseClause.test.value] = caseClause;
        });

        // find all '_context.next = <test>;' assignments
        traverse(ast, stage5, null, context);

        // Insert the new cases, then we can re-index the cases and their '_context.next = ' references
        traverse(ast, stage6, null, context);

        // re-index the case <test> values
        context.switchClause.cases.forEach((caseClause, index) => {
            caseClause.test.value = index;
        });

        // and update all '_context.next = <test>;' assignments 
        traverse(ast, stage7, null, context);

        // we are fucking done !

        return generate(ast, {
            sourceFileName: 'lingo_module.js',
            sourceType: 'script',
            sourceMaps: true,

        });
    } catch (ex) {
        let errMsg = ex.message.split('\n')[0];
        errMsg = errMsg.replace(/\(\d+:\d+\)/g, '');
        errMsg = errMsg.replace(/^(undefined|unknown):\s*/, '');

        return {
            error: {
                message: errMsg,     // Trim off the source code highlights as the line numbers are incorrect due to the prefixed code
                detailMessage: ex.message,
                pos: ex.pos - CODE_PREFIX.length,
                loc: {
                    line: ex.loc.line - 1,          // Adjust for the prefixed code
                    column: ex.loc.column
                }
            }
        }
    }
}

module.exports = {
    lingoJsTranspile
}