const lingoJsTranspile = require('./index').lingoJsTranspile;
const Chalk = require('chalk');
const chalk = new Chalk.Instance({ level: 2 });

const bluebird = require('bluebird');
const fs = bluebird.promisifyAll(require('fs'));

const readline = require('readline');

function askQuestion(query) {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });

    return new Promise(resolve => rl.question(query, ans => {
        rl.close();
        resolve(ans);
    }))
}

let filename = `./test/Lingo.js`;

async function transpile(filename) {
    let source = await fs.readFileAsync(filename, 'utf8');

    console.log(chalk.yellow(source));
    let output = await lingoJsTranspile(source);

    if (output.error) {
        console.log(chalk.red(`${output.error.message}\n${output.error.detailMessage}`));
    } else {
        output.map.file = 'Lingo.min.js';
        output.map.sources = ['Lingo.js'];

        await fs.writeFileAsync(`${filename}.min`, output.code, 'utf8');
        await fs.writeFileAsync(`${filename}.map`, JSON.stringify(output.map), 'utf8');

        console.log(chalk.green(output.code));
    }
}

async function go() {
    fs.watchFile(filename, { persistent: false }, async () => {
        await transpile(filename);
    });

    await transpile(filename);
    await askQuestion(`Press Enter key to finish`);
}

go();
